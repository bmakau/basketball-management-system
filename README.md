# Basketball Management System(BMS)
The system would manage the basic data associated with a basketball team(General Managers, Coaches, Players) and allow each entity to be viewed and modified accordingly. Users can login as a General Manager, a Coach, or a Player. A General Manager would be able to trade/cut/sign players, fire/hire coaches, get team/player stats, and change the team name or location. A Coach would be able to modify players’ minutes and position as well as create a lineup of 5 players that would be utilized in a game. A player may modify their name, height, and weight to reflect any changes in their physique, request trades, and get their personal stats.

##  Users:
*  Register
*  Login/Logout. ·
*  Get team/player stats. ·
*  Create teams
*   View personnel. ·
*   Update player team assignments.

##  Players
*  Request trades
*  changes names, height, weight
*  get personal stats

## coaches
*  modify player minutes
*  modify players positions
*  change lineups
*  assign practices dates
## general managers
*   trade players
*  sign/cut players
*   fire hire coaches
*  change team name/location
*  get team/player stats
## Teams
*  12 players minimum
1 of each position at least(PG,SG,SF,PF,C)
*   Location, name , number of players, number of coaches GM_id, Field
##  Players
id ,name, Height, weight, points per game, assist per game, steals and block per game
##  Coaches
coach id, name, experience in years, Team-id
##  General managers
gm_id, Gm name
## Lineup 
Lineup id
##  Team Members: Darius Johnson, Boniface Makau, Jad Al Wazzan, Chris Wolfe-McGuire


